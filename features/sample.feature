# Feature file to be used with cucumber-puppet
Feature: Sample_Resource is version x.x
    As a valid node
    I need to be able to run sample_resource

Scenario: Checking that sample_resource is the expected version
    When I run '/usr/bin/sample_resource --version'
    Then I should see "x.x" in the stdout
